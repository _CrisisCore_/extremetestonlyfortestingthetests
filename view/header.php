<!DOCTYPE html>
<html>

    <!-- the head section -->
    <head>
        <title>Gaming Rendezvous</title>
        <link rel="stylesheet" type="text/css"
              href="/GamingRendezvous/main.css">
    <img src="/GamingRendezvous/images/uscb_logo.png" alt="USCB Logo" align="right" height="120px" width="160px">
</head>

<!-- the body section -->
<body>
    <header>
        <h1>Gaming Rendezvous</h1>
        <p>Your hub for a great social experience with game development!</p>
        <nav>
            <ul>
                <li><a href="/GamingRendezvous/">Home</a></li>
            </ul>
        </nav>
    </header>
