<?php
    $dsn = 'mysql:host=localhost;dbname=test';
    $username = 'root';
    $password = ''; // or change to whatever your root password is
    
    try {
        $db = new PDO($dsn, $username, $password);
    } catch (PDOException $e) {
        $error_message = $e->getMessage();
        include('../errors/database_error.php');
        exit();
    }
?>
